export default {
    criticalCodes: [500],
    isCriticalCode(code) {
        return this.criticalCodes.indexOf(code) !== -1;
    }
}