export default {
    map(inputControls, field) {
        let mapped = {};

        for(let key in inputControls) {
            mapped[key] = inputControls[key][field];
        }

        return mapped;
    },
    mapValues(inputControls) {
        return this.map(inputControls, 'value');
    },
    setErrors(inputControls, errors) {
        for(let key in inputControls) {
            inputControls[key].errors = errors[key] ? errors[key] : [];
        }
    }
}